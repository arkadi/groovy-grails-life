package week1

class Minefield {

    static void main(String[] args) {
        println("Enter minefield:")
        print('\n' + hint(System.in.readLines().join('\n')))
    }

    static String hint(String _field) {
        def field = parse(_field)
        def dim = dimensions(field)
        // Groovy has no zipWithIndex()
        def hints = new char[dim.y][dim.x]
        field.eachWithIndex { line, y ->
            (0..<dim.x).each { x ->
                hints[y][x] = hintCell(field, dim, x, y)
            }
        }
        hints.collect { new String(it) } .join('\n') + '\n'
    }

    static String[] parse(String field) {
        def lines = field.split('\r?\n').grep()
        if (!lines) throw new IllegalArgumentException("No input data")
        def cols = lines.collect { it.length() } .unique()
        if (cols.size() > 1 || lines.any { (it =~ /[^\*\.]/).find() })
            throw new IllegalArgumentException("Bad input data")
        lines
    }

    static def hintCell(field, dim, int x, int y) {
        char c = field[y][x]
        if (c == '*') c
        else
            ('0' as char) + neighbours(cell(x, y), dim).inject(0) { total, neighbour ->
                total + (field[neighbour.y][neighbour.x] == '*' ? 1 : 0)
            } as char
    }

    static def cell(int x, int y) { [ x: x, y: y ] }
    static def cell(List<Integer> coords) { cell(coords[0], coords[1]) }

    static def dimensions(String[] field) { cell(field[0].length(), field.size()) }

    static def neighbours(c, dim) {
        [
            [c.x-1, c.y+1], [c.x, c.y+1], [c.x+1, c.y+1],
            [c.x-1, c.y],                 [c.x+1, c.y],
            [c.x-1, c.y-1], [c.x, c.y-1], [c.x+1, c.y-1]
        ] .collect { cell(it) }
          .findAll { cell -> cell.x >= 0 && cell.x < dim.x && cell.y >= 0 &&  cell.y < dim.y }
    }
}
