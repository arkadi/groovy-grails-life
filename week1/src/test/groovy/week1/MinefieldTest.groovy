package week1

import static Minefield.*
import org.junit.Test

@org.junit.runner.RunWith(org.junit.runners.JUnit4.class)
class UnitTests extends GroovyTestCase {

    def field = '''
                |*.*.
                |....
                |...*
                |'''.stripMargin()

    def hinted = '''
                |*2*1
                |1222
                |001*
                |'''.stripMargin().substring(1)

    @Test
    void testParse() {
        assert ['*.*.', '....', '...*'] == parse(field)
    }

    @Test
    void testBadParse() {
        shouldFail { parse('z') }
        shouldFail { parse('..\n*') }
    }

    @Test
    void testHintCell() {
        def parsed = parse(field)
        def dim = dimensions(parsed)
        assert '*' == hintCell(parsed, dim, 0, 0)
        assert '2' == hintCell(parsed, dim, 1, 0)
        assert '1' == hintCell(parsed, dim, 2, 2)
        assert '0' == hintCell(parsed, dim, 0, 2)
    }

    @Test
    void testHint() {
        assert hinted == hint(field)
    }
}
