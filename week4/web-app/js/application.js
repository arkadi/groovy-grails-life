if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

// load Google+ API
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

window.$app = (function () {
        var access_token = undefined
        return {
            update_login: function(name, surname, login) {
                function propagate() { login.val([name.val(), surname.val()].join('.').toLowerCase()) }
                login.change(function() { [name, surname].map(function(field) { field.off('change') }) })
                name.change(propagate)
                surname.change(propagate)
            },

            signin_callback: function(auth, csrf_token) {
                ['code', 'access_token', 'token_type', 'expires_in', 'error'].map(function (k) { console.log(k + ' = |' + auth[k] + '|') })
                if (!auth['error']) {
                    access_token = auth.access_token
                    $.post('plus', { code: auth.code, csrf_token: csrf_token }).done(function (response) {
                        console.log(response)
                        $('#response').html(response)
                    })
                }
            }
        }
    })();
