
<%@ page import="week4.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="foundation">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <h1><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
            </ul>
            <section class="top-bar-section">
                <ul class="left">
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
                <ul class="right">
                    <li><g:link controller="auth" action="signout">Sign out</g:link></li>
                </ul>
            </section>
        </nav>
		<div id="list-user" class="content scaffold-list" role="main">
			<h3><g:message code="default.list.label" args="[entityName]" /></h3>
			<g:if test="${flash.message}">
				<div class="alert-box success round" role="status">${flash.message}</div>
			</g:if>
            <g:form url="[resource:userInstance, action:'delete']" method="DELETE">

                <div class="large-8 columns">
                    <div class="row">
                        <g:each in="${userInstanceList}" status="i" var="userInstance">

                            <div class="large-4 small-6 columns">
                                <g:if test="userInstance.thumb">
                                    <img src="data:image/jpeg;base64,${userInstance.thumbBase64()}" />
                                </g:if>
                                <g:else>
                                    <img src="${createLink(action: 'thumb', id: userInstance.id)}" />
                                </g:else>

                                <div class="panel">
                                    <h5><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "name")} ${fieldValue(bean: userInstance, field: "surname")}</g:link></h5>
                                    <h6 class="subheader">${fieldValue(bean: userInstance, field: "login")} <g:radio name="id" value="${userInstance.id}"/></h6>
                                </div>
                            </div>
                        </g:each>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
                            <g:actionSubmit class="button tiny alert radius" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </div>
                    </div>
                </div>

            </g:form>
		</div>
	</body>
</html>
