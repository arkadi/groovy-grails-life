<%@ page import="week4.User" %>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="user.name.label" default="Name" />
	</label>
	<g:textField name="name" value="${userInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'surname', 'error')} ">
	<label for="surname">
		<g:message code="user.surname.label" default="Surname" />
	</label>
	<g:textField name="surname" value="${userInstance?.surname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'login', 'error')} ">
    <label for="login">
        <g:message code="user.login.label" default="Login" />
    </label>
    <g:textField name="login" maxlength="64" value="${userInstance?.login}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'photo', 'error')} ">
    <label for="photo.content">
        <g:message code="user.photo.label" default="Photo" />
    </label>
    <input type="file" name="photo.content" />
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#name").focus();
        $app.update_login($("#name"), $("#surname"), $("#login"));
    })
</script>
