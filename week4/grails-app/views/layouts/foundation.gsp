<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><g:layoutTitle default="Grails"/></title>
    <r:require module="modernizr"/>
    <r:require modules="foundation"/>
    <g:layoutHead/>
    <r:layoutResources/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'application.css')}" type="text/css">
    <g:javascript library="application"/>
    <!-- workaround buggy plugin in dev mode -->
    <script type="text/javascript" src="${resource(dir: 'js', file: 'foundation.js')}"></script>
</head>
<body>
    <div class="row">
        <div id="spinner" class="spinner" ><g:message code="spinner.alt" default="Loading&hellip;"/></div>
        <g:layoutBody/>
     </div>
    <r:layoutResources/>
    <script type="text/javascript">
        $(document).foundation();
    </script>
</body>
</html>
