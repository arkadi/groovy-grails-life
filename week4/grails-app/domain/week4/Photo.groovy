package week4

class Photo {
    static belongsTo = [user: User]
    byte[] content
    static constraints = {
        content     maxSize: 10_000_000
    }
}
