package week4

class User {
    String login
    String name
    String surname
    static hasOne = [photo: Photo]
  //Photo  photo
    byte[] thumb
    Date   dateCreated
    Date   lastUpdated

    def thumbBase64() {
        javax.xml.bind.DatatypeConverter.printBase64Binary(thumb)
    }

    static constraints = {
        login       maxSize: 64, unique: true, index: true
        name        nullable: true
        surname     nullable: true
        photo       nullable: true
        thumb       maxSize: 10_000, nullable: true
        lastUpdated nullable: true
    }
}

/*
mysql> desc user;
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| id           | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| version      | bigint(20)   | NO   |     | NULL    |                |
| date_created | datetime     | NO   |     | NULL    |                |
| last_updated | datetime     | YES  |     | NULL    |                |
| login        | varchar(64)  | NO   | UNI | NULL    |                |
| name         | varchar(255) | YES  |     | NULL    |                |
| surname      | varchar(255) | YES  |     | NULL    |                |
| thumb        | blob         | YES  |     | NULL    |                |
+--------------+--------------+------+-----+---------+----------------+
8 rows in set (0.00 sec)

mysql> desc photo;
+---------+------------+------+-----+---------+----------------+
| Field   | Type       | Null | Key | Default | Extra          |
+---------+------------+------+-----+---------+----------------+
| id      | bigint(20) | NO   | PRI | NULL    | auto_increment |
| version | bigint(20) | NO   |     | NULL    |                |
| content | mediumblob | NO   |     | NULL    |                |
| user_id | bigint(20) | NO   | UNI | NULL    |                |
+---------+------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)
*/
