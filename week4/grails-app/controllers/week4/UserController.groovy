package week4


import static org.springframework.http.HttpStatus.*
import static grails.async.Promises.*
import grails.transaction.Transactional

import java.awt.Image
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

/**
 * UserController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

	def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true

//        request.withFormat {
//            uploadForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userInstance.label', default: 'User'), userInstance.login])
                redirect action:"index", method:"GET"
//            }
//            '*' { respond userInstance, [status: CREATED] }
//        }
    }

    def thumb(User userInstance) {
        if (userInstance.photo == null) // pull in large binary synchronously
            redirect url: "http://placehold.it/256x256&text=${userInstance.name}"
        else {
            userInstance.discard()
            task {
                int width = 256

                def photo = ImageIO.read(new ByteArrayInputStream(userInstance.photo.content))
                def thumb = photo.getScaledInstance(width, -1, Image.SCALE_SMOOTH)
                def bthumb = new BufferedImage(thumb.getWidth(null), thumb.getHeight(null), BufferedImage.TYPE_INT_RGB) // jpeg doesn't like argb
                bthumb.graphics.drawImage(thumb, 0, 0, null)
                def bos = new ByteArrayOutputStream()
                ImageIO.write(bthumb, "jpeg", bos)
                bos.close()

                def bin = bos.toByteArray()
                User.withNewSession {
                    userInstance.attach()
                    userInstance.thumb = bin
                    userInstance.save flush: true
                }

                render contentType: 'image/jpeg', file: new ByteArrayInputStream(bin)
            }
        }
    }

    def edit(User userInstance) {
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.login])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.login])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInstance.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
