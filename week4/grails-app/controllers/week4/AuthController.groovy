package week4

import wslite.rest.*

class AuthController {

    String redirectUri = 'http://localhost:8080/week4/auth/oauth2callback'
    String clientId = '435144070273-4if06hh5rec9n5uu2ac69lijo0d7d68b.apps.googleusercontent.com'
    String clientSecret = ''

    def accounts = new RESTClient("https://accounts.google.com")
    def apis = new RESTClient("https://www.googleapis.com")

    def index() {
        String token = UUID.randomUUID().toString()
        session.csrf_token = token
        [ csrf_token: token ]
    }

    private def forbidden() { render(status: response.SC_FORBIDDEN, text: 'Forbidden') }

    def plus(String code, String csrf_token) {
        // check client against session
        if (!session.csrf_token || session.csrf_token != csrf_token) {
            forbidden(); return
        }
        session.removeAttribute('csrf_token')

        // exchange code for (server) access token and refresh token
        def tokens = accounts.post(path: '/o/oauth2/token') {
                urlenc code: code, redirect_uri: 'postmessage', client_id: clientId, scope: '', client_secret: clientSecret, grant_type: 'authorization_code'
            }
        def u = tokens.json
        session.user = u

        // request user information and email
        def account = apis.get(path: '/oauth2/v2/userinfo', headers: [ Authorization: "${u.token_type} ${u.access_token}" ])
        account.json
    }

    def signout() {
        session.removeAttribute('user')
        redirect(action: "index")
    }
}
