
<%@ page import="week2.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="foundation">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <h1><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
            </ul>
            <section class="top-bar-section">
                <ul class="left">
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
            </section>
        </nav>
		<div id="list-user" class="content scaffold-list" role="main">
			<h3><g:message code="default.list.label" args="[entityName]" /></h3>
			<g:if test="${flash.message}">
				<div class="alert-box success round" role="status">${flash.message}</div>
			</g:if>
            <g:form url="[resource:userInstance, action:'delete']" method="DELETE">
                <table>
                    <thead>
                        <tr>

                            <g:sortableColumn property="login" title="${message(code: 'user.login.label', default: 'Login')}" />

                            <g:sortableColumn property="name" title="${message(code: 'user.name.label', default: 'Name')}" />

                            <g:sortableColumn property="surname" title="${message(code: 'user.surname.label', default: 'Surname')}" />

                            <g:sortableColumn property="dateCreated" title="${message(code: 'user.dateCreated.label', default: 'Date Created')}" />

                        </tr>
                    </thead>
    				<tbody>
                        <g:each in="${userInstanceList}" status="i" var="userInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "login")}</g:link></td>

                                <td>${fieldValue(bean: userInstance, field: "name")}</td>

                                <td>${fieldValue(bean: userInstance, field: "surname")}</td>

                                <td><g:formatDate date="${userInstance.dateCreated}" /></td>

                                <td><g:radio name="id" value="${userInstance.id}"/></td>

                            </tr>
                        </g:each>
                    </tbody>
    			</table>
                <div class="pagination">
                    <g:paginate total="${userInstanceCount ?: 0}" />
                </div>
                <g:actionSubmit class="button tiny alert radius" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </g:form>
		</div>
	</body>
</html>
