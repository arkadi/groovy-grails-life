<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="foundation"/>
		<title>Welcome to Grails</title>
	</head>
	<body>
        <div class="row">
            <div class="large-8 columns">
                <h3 class="title">Welcome to Grails</h3>
                <img src="${createLinkTo(dir: 'images', file: 'grails_logo.png')}" style="float:left; margin-right: 1em;"/>
                <p>Congratulations, you have successfully started your first Grails application! At the moment
                this is the default page, feel free to modify it to either redirect to a controller or display whatever
                content you may choose. On the right is a list of controllers that are currently deployed in this application,
                click on each to execute its default action.</p>
            </div>
            <div class="large-4 columns">
                <h5 class="title">Available Controllers:</h5>
                <ul>
                    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
                        <li class="controller"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>
                    </g:each>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns">
                <h5 class="title">Application Status</h5>
                <ul>
                    <li>App version: <g:meta name="app.version"/></li>
                    <li>Grails version: <g:meta name="app.grails.version"/></li>
                    <li>Groovy version: ${GroovySystem.getVersion()}</li>
                    <li>JVM version: ${System.getProperty('java.version')}</li>
                    <li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
                    <li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
                    <li>Domains: ${grailsApplication.domainClasses.size()}</li>
                    <li>Services: ${grailsApplication.serviceClasses.size()}</li>
                    <li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
                </ul>
            </div>
            <div class="large-6 columns">
                <h5 class="title">Installed Plugins</h5>
                <ul>
                    <g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins.sort { it.name } }">
                        <li>${plugin.name} - ${plugin.version}</li>
                    </g:each>
                </ul>
            </div>
		</div>
	</body>
</html>
