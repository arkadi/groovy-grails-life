package week2

import spock.lang.*

class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void 'test User domain class'() {
        def name = 'random'

        when: 'valid object is saved'
        def user = new User(login: 'test', name: name)
        user.save(flush: true)

        then: 'it could be read back'
        def dbuser = User.findByLogin('test')

        expect:
        name == dbuser.name

        when: 'valid duplicate is saved'
        def user2 = new User(login: 'test', name: name)
        user2.insert(flush: true, failOnError: true)

        then: 'exception is raised'
        thrown(grails.validation.ValidationException)
    }
}
