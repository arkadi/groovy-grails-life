if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

window.$app = {
    update_login: function(name, surname, login) {
        function propagate() { login.val([name.val(), surname.val()].join('.').toLowerCase()) }
        login.change(function() { [name, surname].map(function(field) { field.off('change') }) })
        name.change(propagate)
        surname.change(propagate)
    }
}

