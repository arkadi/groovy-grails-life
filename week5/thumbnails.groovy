@Grapes([
    @Grab(group='org.codehaus.gpars', module='gpars', version='1.1.0')])

import static groovyx.gpars.GParsPool.runForkJoin
import static groovyx.gpars.GParsPool.withPool

import java.awt.Image
import java.awt.image.BufferedImage
import javax.imageio.ImageIO


images = ~/(.+)\.(?i)jpe?g$/

def main() {
    if (args.size() != 1) usage()
    def baseDir = new File(args[0])
    if (!baseDir.isDirectory()) usage()

    def start = System.nanoTime()

    withPool() {
        int created =
            runForkJoin(baseDir) { file ->
                if (file.isDirectory()) {
                    file.eachFile { sub ->
                        if (sub.isDirectory() || images.matcher(sub.name).matches())
                            forkOffChild(sub)
                    }
                    childrenResults.sum(0)
                } else {
                    boolean resized = false
                    try {
                        resize(file)
                        resized = true
                    } catch (Exception e) {
                        println("Unable to create thumbnail for ${file.path}: ${e.message}")
                    }
                    resized ? 1 : 0
                }
            }

        def elapsed = (System.nanoTime() - start)/1_000_000 as int
        println "Created $created thumbnails, took ${elapsed}ms"
    }
}

def usage() {
    println "usage: groovy thumbnails.groovy <path to JPG dir>"
    System.exit(1)
}

def resize(File source) {
    int width = 128
    def photo = ImageIO.read(source)
    if (!photo) throw new RuntimeException("Broken image")
    def thumb = photo.getScaledInstance(width, -1, Image.SCALE_SMOOTH)
    def bthumb = new BufferedImage(thumb.getWidth(null), thumb.getHeight(null), BufferedImage.TYPE_INT_RGB) // jpeg doesn't like argb
    bthumb.graphics.drawImage(thumb, 0, 0, null)
    def out = new FileOutputStream(images.matcher(source.absolutePath)[0][1] + ".thumb.jpg")
    ImageIO.write(bthumb, "jpeg", out)
    out.close()
}

main()
