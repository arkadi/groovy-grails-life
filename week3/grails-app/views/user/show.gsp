
<%@ page import="week3.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="foundation">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <h1><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
            </ul>
            <section class="top-bar-section">
                <ul class="left">
                    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </ul>
                <ul class="right">
                    <li><g:link controller="auth" action="signout">Sign out</g:link></li>
                </ul>
            </section>
        </nav>
		<div id="show-user" class="content scaffold-show" role="main">
			<h3><g:message code="default.show.label" args="[entityName]" /></h3>
			<g:if test="${flash.message}">
			    <div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list user">

				<g:if test="${userInstance?.name}">
				<li class="fieldcontain">
                    <span class="property-value"><g:fieldValue bean="${userInstance}" field="name"/></span>
				</li>
				</g:if>

				<g:if test="${userInstance?.surname}">
				<li class="fieldcontain">
                    <span class="property-value"><g:fieldValue bean="${userInstance}" field="surname"/></span>
				</li>
				</g:if>

                <g:if test="${userInstance?.login}">
                    <li class="fieldcontain">

                        <span class="property-value" aria-labelledby="login-label"><g:fieldValue bean="${userInstance}" field="login"/></span>
                        <span id="login-label" class="label radius"><g:message code="user.login.label" default="Login" /></span>

                    </li>
                </g:if>

				<g:if test="${userInstance?.dateCreated}">
				<li class="fieldcontain">
                    <span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${userInstance?.dateCreated}" /></span>
				</li>
				</g:if>

			</ol>
			<g:form url="[resource:userInstance, action:'delete']" method="DELETE">
                <g:link class="edit" action="edit" resource="${userInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>&nbsp;
                <g:actionSubmit class="button tiny alert radius" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</g:form>
		</div>
	</body>
</html>
