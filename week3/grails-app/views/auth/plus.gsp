<div class="large-8 columns">
    <p>Great! You've been logged in.</p>
    <p>Please proceed to the <g:link uri="/">main index</g:link>.</p>
</div>
<g:if test="${name}">
    <div class="row">
        <div class="large-8 columns">
            <g:if test="${link}"><a href="${link}">${name}</a></g:if>
            <g:else>${name}</g:else>
        </div>
    </div>
</g:if>
<g:if test="${email}">
    <div class="row">
        <div class="large-8 columns">
            <a href="mailto:${email}">${email}</a><g:if test="${verified_email}"> (verified)</g:if>
        </div>
    </div>
</g:if>
<g:if test="${picture}">
    <div class="row">
        <div class="large-8 columns">
            <img src="${picture}" />
        </div>
    </div>
</g:if>
