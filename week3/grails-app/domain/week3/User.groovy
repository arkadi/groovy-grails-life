package week3

class User {
    String login
    String name
    String surname
    Date   dateCreated
    Date   lastUpdated

    static constraints = {
        login       maxSize: 64, unique: true, index: true
        name        nullable: true
        surname     nullable: true
        lastUpdated nullable: true
    }
}
