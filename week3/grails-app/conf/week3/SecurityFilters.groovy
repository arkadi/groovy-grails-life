package week3

class SecurityFilters {

    def filters = {
        all(controller:'*', action:'*') {
            before = {
                if (!session.user && controllerName != 'auth')
                    redirect(controller: 'auth', action: 'index')
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}
